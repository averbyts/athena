#include "KLFitterAnalysisAlgorithms/KLFitterResult.h"
#include "KLFitterAnalysisAlgorithms/KLFitterResultAuxContainer.h"
#include "KLFitterAnalysisAlgorithms/KLFitterResultContainer.h"

#ifdef __CINT__

#pragma extra_include "KLFitterAnalysisAlgorithms/KLFitterResult.h";
#pragma extra_include "KLFitterAnalysisAlgorithms/KLFitterResultContainer.h";
#pragma extra_include \
    "KLFitterAnalysisAlgorithms/KLFitterResultAuxContainer.h";

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#pragma link C++ class xAOD::KLFitterResult + ;
#pragma link C++ class xAOD::KLFitterResultContainer + ;
#pragma link C++ class xAOD::KLFitterResultAuxContainer + ;

#endif
