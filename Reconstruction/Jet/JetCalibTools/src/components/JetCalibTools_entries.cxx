#include "JetCalibTools/JetCalibrationTool.h"
#include "JetCalibTools/JetCalibTool.h"
#include "JetCalibTools/PileupAreaCalibStep.h"
#include "JetCalibTools/Pileup1DResidualCalibStep.h"
#include "JetCalibTools/JESCalibStep.h"
#include "JetCalibTools/SmearingCalibStep.h"
#include "JetCalibTools/GSCCalibStep.h"
#include "JetCalibTools/MuonInJetCorrectionTool.h"
#include "JetCalibTools/BJetCorrectionTool.h"

DECLARE_COMPONENT( JetCalibrationTool )
DECLARE_COMPONENT( JetCalibTool )
DECLARE_COMPONENT( PileupAreaCalibStep )
DECLARE_COMPONENT( EtaMassJESCalibStep )
DECLARE_COMPONENT( SmearingCalibStep )
DECLARE_COMPONENT( GSCCalibStep )
DECLARE_COMPONENT( MuonInJetCorrectionTool )
DECLARE_COMPONENT( BJetCorrectionTool )
DECLARE_COMPONENT( Pileup1DResidualCalibStep )
